package lk.code4x.hrsys.service.Impl;

import lk.code4x.hrsys.dto.EmployeeDTO;
import lk.code4x.hrsys.dto.ResponseDTO;
import lk.code4x.hrsys.model.Employee;
import lk.code4x.hrsys.repo.EmployeeRepository;
import lk.code4x.hrsys.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author dilshan.r
 * @created 18/03/2022 - 9:28 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public ResponseDTO employeeRegistration(EmployeeDTO employeeDTO) {

        String address = employeeDTO.getAddress();
        String dateOfBirth = employeeDTO.getdateOfBirth();
        String division = employeeDTO.getDivision();
        String document = employeeDTO.getDocuments();
        String educationLevel = employeeDTO.geteducationLevel();
        String email =  employeeDTO.getEmail();
        String employeeNumber = employeeDTO.getemployeeNumber();
        String freshmanOrNo = employeeDTO.getfreshmanOrNot();
        String fullName = employeeDTO.getfullName();
        String gender = employeeDTO.getGender();
        String involvedDate = employeeDTO.getinvolvedDate();
        String nicNumber = employeeDTO.getnicNumber();
        String telephone = employeeDTO.getTelephone();
        String thumbnail = employeeDTO.getThumbnail();

        Employee employeeByEmail = employeeRepository.getEmployeeByEmail(email);
        Employee employeeByNicNumber = employeeRepository.getEmployeeByNicNumber(nicNumber);

        if (employeeByEmail != null) {
            return new ResponseDTO(
                    false,
                    "This email is already in registered!",
                    employeeByEmail
            );
        } else if (employeeByNicNumber != null) {
            return new ResponseDTO(
                    false,
                    "This nic is already in registered!",
                    employeeByNicNumber
            );
        } else {
            if (address.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee address is required"
                );
            } else if (dateOfBirth.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee date of birth is required"
                );
            } else if (division.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee division is required"
                );
            } else if (document.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee documents are required"
                );
            } else if (educationLevel.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee education levels are required"
                );
            } else if (email.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee email address is required"
                );
            } else if (employeeNumber.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee employee number is required"
                );
            } else if (freshmanOrNo.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee freshman state is required"
                );
            } else if (fullName.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee full name is required"
                );
            } else if (gender.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee gender is required"
                );
            } else if (involvedDate.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee involved date is required"
                );
            } else if (nicNumber.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee nic number is required"
                );
            } else if (telephone.equalsIgnoreCase("")) {
                return new ResponseDTO(
                        false,
                        "Employee telephone number is required"
                );
            } else {

                try {
                    Employee employee = new Employee();

                    employee.setAddress(address);
                    employee.setDateOfBirth(dateOfBirth);
                    employee.setDivision(division);
                    employee.setDocuments(document);
                    employee.setEducationLevel(educationLevel);
                    employee.setEmail(email);
                    employee.setEmployeeNumber(employeeNumber);
                    employee.setFreshmanOrNot(freshmanOrNo);
                    employee.setFullName(fullName);
                    employee.setGender(gender);
                    employee.setInvolvedDate(involvedDate);
                    employee.setNicNumber(nicNumber);
                    employee.setTelephone(telephone);
                    employee.setThumbnail(thumbnail);

                    employeeRepository.save(employee);

                    EmployeeDTO registeredEmployee = new EmployeeDTO();

                    registeredEmployee.setemployeeId(employee.getEmployeeId());
                    registeredEmployee.setAddress(employee.getAddress());
                    registeredEmployee.setdateOfBirth(employee.getDateOfBirth());
                    registeredEmployee.setDivision(employee.getDivision());
                    registeredEmployee.setDocuments(employee.getDocuments());
                    registeredEmployee.seteducationLevel(employee.getEducationLevel());
                    registeredEmployee.setEmail(employee.getEmail());
                    registeredEmployee.setemployeeNumber(employee.getEmployeeNumber());
                    registeredEmployee.setfreshmanOrNot(employee.getFreshmanOrNot());
                    registeredEmployee.setfullName(employee.getFullName());
                    registeredEmployee.setGender(employee.getGender());
                    registeredEmployee.setinvolvedDate(employee.getInvolvedDate());
                    registeredEmployee.setnicNumber(employee.getNicNumber());
                    registeredEmployee.setTelephone(employee.getTelephone());
                    registeredEmployee.setThumbnail(employee.getThumbnail());

                    return new ResponseDTO(
                            true,
                            registeredEmployee.getfullName() + " added successfully!",
                            registeredEmployee
                    );

                } catch (Exception e) {
                    return new ResponseDTO(
                            false,
                            "Something went wrong. Please try again!"
                    );
                }

            }
        }
    }

    @Override
    public ResponseDTO employeeUpdate(EmployeeDTO employeeDTO, Long employeeId) {

        String address = employeeDTO.getAddress();
        String dateOfBirth = employeeDTO.getdateOfBirth();
        String division = employeeDTO.getDivision();
        String document = employeeDTO.getDocuments();
        String educationLevel = employeeDTO.geteducationLevel();
        String email =  employeeDTO.getEmail();
        String employeeNumber = employeeDTO.getemployeeNumber();
        String freshmanOrNo = employeeDTO.getfreshmanOrNot();
        String fullName = employeeDTO.getfullName();
        String gender = employeeDTO.getGender();
        String involvedDate = employeeDTO.getinvolvedDate();
        String nicNumber = employeeDTO.getnicNumber();
        String telephone = employeeDTO.getTelephone();
        String thumbnail = employeeDTO.getThumbnail();

        Optional<Employee> employeeByID = employeeRepository.findById(employeeId);

        if (!employeeByID.isPresent()) {
            return new ResponseDTO(
                    false,
                    "Cannot find the employee you searched"
            );
        } else {
            try {
                Employee employee = employeeByID.get();

                if (address.equalsIgnoreCase("")) {
                    return new ResponseDTO(
                            false,
                            "Employee address is required"
                    );
                } else if (document.equalsIgnoreCase("")) {
                    return new ResponseDTO(
                            false,
                            "Employee documents are required"
                    );
                } else if (educationLevel.equalsIgnoreCase("")) {
                    return new ResponseDTO(
                            false,
                            "Employee education level is required"
                    );
                } else if (email.equalsIgnoreCase("")) {
                    return new ResponseDTO(
                            false,
                            "Employee email is required"
                    );
                } else if (fullName.equalsIgnoreCase("")) {
                    return new ResponseDTO(
                            false,
                            "Employee full name is required"
                    );
                } else if (telephone.equalsIgnoreCase("")) {
                    return new ResponseDTO(
                            false,
                            "Employee telephone number is required"
                    );
                } else {

                    EmployeeDTO checkEmployee = new EmployeeDTO();

                    checkEmployee.setAddress(address);
                    checkEmployee.setDocuments(document);
                    checkEmployee.seteducationLevel(educationLevel);
                    checkEmployee.setEmail(email);
                    checkEmployee.setfullName(fullName);
                    checkEmployee.setTelephone(telephone);
                    checkEmployee.setThumbnail(thumbnail);

                    if (employee.getAddress().equalsIgnoreCase(checkEmployee.getAddress()) &&
                            employee.getDocuments().equalsIgnoreCase(checkEmployee.getDocuments()) &&
                            employee.getEducationLevel().equalsIgnoreCase(checkEmployee.geteducationLevel()) &&
                            employee.getEmail().equalsIgnoreCase(checkEmployee.getEmail()) &&
                            employee.getFullName().equalsIgnoreCase(checkEmployee.getfullName()) &&
                            employee.getTelephone().equalsIgnoreCase(checkEmployee.getTelephone()) &&
                            employee.getThumbnail().equalsIgnoreCase(checkEmployee.getThumbnail())
                    ) {
                        return new ResponseDTO(
                                true,
                                "Cannot found any updated field of " + employee.getFullName()
                        );
                    } else {
                        employee.setAddress(address);
                        employee.setDateOfBirth(dateOfBirth);
                        employee.setDivision(division);
                        employee.setDocuments(document);
                        employee.setEducationLevel(educationLevel);
                        employee.setEmail(email);
                        employee.setEmployeeNumber(employeeNumber);
                        employee.setFreshmanOrNot(freshmanOrNo);
                        employee.setFullName(fullName);
                        employee.setGender(gender);
                        employee.setInvolvedDate(involvedDate);
                        employee.setNicNumber(nicNumber);
                        employee.setTelephone(telephone);
                        employee.setThumbnail(thumbnail);

                        employeeRepository.save(employee);

                        EmployeeDTO updatedEmployee = new EmployeeDTO();

                        updatedEmployee.setemployeeId(employee.getEmployeeId());
                        updatedEmployee.setAddress(employee.getAddress());
                        updatedEmployee.setdateOfBirth(employee.getDateOfBirth());
                        updatedEmployee.setDivision(employee.getDivision());
                        updatedEmployee.setDocuments(employee.getDocuments());
                        updatedEmployee.seteducationLevel(employee.getEducationLevel());
                        updatedEmployee.setEmail(employee.getEmail());
                        updatedEmployee.setemployeeNumber(employee.getEmployeeNumber());
                        updatedEmployee.setfreshmanOrNot(employee.getFreshmanOrNot());
                        updatedEmployee.setfullName(employee.getFullName());
                        updatedEmployee.setGender(employee.getGender());
                        updatedEmployee.setinvolvedDate(employee.getInvolvedDate());
                        updatedEmployee.setnicNumber(employee.getNicNumber());
                        updatedEmployee.setTelephone(employee.getTelephone());
                        updatedEmployee.setThumbnail(employee.getThumbnail());

                        return new ResponseDTO(
                                true,
                                employeeDTO.getfullName() + " detail update successfully!",
                                updatedEmployee
                        );
                    }
                }
            } catch (Exception e) {
                return new ResponseDTO(
                        false,
                        "Something went wrong. Please try again!"
                );
            }
        }
    }

    @Override
    public ResponseDTO employeeDelete(Long employeeId) {
        try {
            Optional<Employee> employeeById = employeeRepository.findById(employeeId);

            if (employeeById.isPresent()) {
                try {
                    Employee employee = employeeById.get();

                    EmployeeDTO employeeDTO = new EmployeeDTO();

                    employeeDTO.setfullName(employee.getFullName());
                    employeeDTO.setEmail(employee.getEmail());
                    employeeDTO.setTelephone(employee.getTelephone());
                    employeeDTO.setDivision(employee.getDivision());
                    employeeDTO.setemployeeNumber(employee.getEmployeeNumber());
                    employeeDTO.setnicNumber(employee.getNicNumber());
                    employeeDTO.setThumbnail(employee.getThumbnail());

                    employeeRepository.delete(employee);

                    return new ResponseDTO(
                            true,
                            employee.getFullName() + " successfully deleted. Thank you!",
                            employeeDTO
                    );

                } catch (Exception exception) {
                    return new ResponseDTO(
                            false,
                            "Cannot delete this employee. Please try again"
                    );
                }
            } else {
                return new ResponseDTO(
                        false,
                        "Sorry, Cannot find employee to delete!"
                );
            }
        } catch (Exception exception) {
            return new ResponseDTO(
                    false,
                    "Something went to wrong. Please try again"
            );
        }
    }

    @Override
    public ResponseDTO employeeGetAll() {

        List<Employee> allEmployees = employeeRepository.findAll();

        if (allEmployees.isEmpty()) {

            return new ResponseDTO(
                    false,
                    "Cannot find any employee!"
            );

        } else {

            ArrayList<EmployeeDTO> employeeList = new ArrayList<>();

            for (Employee employee :  allEmployees) {
                EmployeeDTO employeeDTO = new EmployeeDTO();

                employeeDTO.setemployeeId(employee.getEmployeeId());
                employeeDTO.setAddress(employee.getAddress());
                employeeDTO.setdateOfBirth(employee.getDateOfBirth());
                employeeDTO.setDivision(employee.getDivision());
                employeeDTO.setDocuments(employee.getDocuments());
                employeeDTO.seteducationLevel(employee.getEducationLevel());
                employeeDTO.setEmail(employee.getEmail());
                employeeDTO.setemployeeNumber(employee.getEmployeeNumber());
                employeeDTO.setfreshmanOrNot(employee.getFreshmanOrNot());
                employeeDTO.setfullName(employee.getFullName());
                employeeDTO.setGender(employee.getGender());
                employeeDTO.setinvolvedDate(employee.getInvolvedDate());
                employeeDTO.setnicNumber(employee.getNicNumber());
                employeeDTO.setTelephone(employee.getTelephone());
                employeeDTO.setThumbnail(employee.getThumbnail());

                employeeList.add(employeeDTO);
            }

            return new ResponseDTO(
                    true,
                    "Fetched the all employees!",
                    employeeList
            );
        }
    }

    @Override
    public ResponseDTO employeeSearch(String keyword) {

        List<Employee> searchEmployee = employeeRepository.searchEmployee(keyword);

        if (searchEmployee.isEmpty()) {

            return new ResponseDTO(
                    false,
                    "Cannot find any employee!"
            );

        } else {

            ArrayList<EmployeeDTO> employeeList = new ArrayList<>();

            for (Employee employee :  searchEmployee) {
                EmployeeDTO employeeDTO = new EmployeeDTO();

                employeeDTO.setemployeeId(employee.getEmployeeId());
                employeeDTO.setAddress(employee.getAddress());
                employeeDTO.setdateOfBirth(employee.getDateOfBirth());
                employeeDTO.setDivision(employee.getDivision());
                employeeDTO.setDocuments(employee.getDocuments());
                employeeDTO.seteducationLevel(employee.getEducationLevel());
                employeeDTO.setEmail(employee.getEmail());
                employeeDTO.setemployeeNumber(employee.getEmployeeNumber());
                employeeDTO.setfreshmanOrNot(employee.getFreshmanOrNot());
                employeeDTO.setfullName(employee.getFullName());
                employeeDTO.setGender(employee.getGender());
                employeeDTO.setinvolvedDate(employee.getInvolvedDate());
                employeeDTO.setnicNumber(employee.getNicNumber());
                employeeDTO.setTelephone(employee.getTelephone());
                employeeDTO.setThumbnail(employee.getThumbnail());

                employeeList.add(employeeDTO);
            }

            return new ResponseDTO(
                    true,
                    "Fetched the all employees!",
                    employeeList
            );
        }
    }

}
