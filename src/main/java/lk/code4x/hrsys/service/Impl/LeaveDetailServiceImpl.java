package lk.code4x.hrsys.service.Impl;

import lk.code4x.hrsys.dto.LeaveDetailsDTO;
import lk.code4x.hrsys.dto.ResponseDTO;
import lk.code4x.hrsys.model.Em_Leave;
import lk.code4x.hrsys.model.Employee;
import lk.code4x.hrsys.model.Leave_Details;
import lk.code4x.hrsys.repo.EmployeeRepository;
import lk.code4x.hrsys.repo.LeaveDetailRepository;
import lk.code4x.hrsys.repo.LeaveRepository;
import lk.code4x.hrsys.service.LeaveDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author dilshan.r
 * @created 30/03/2022 - 12:02 PM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Service
public class LeaveDetailServiceImpl implements LeaveDetailService {

    @Autowired
    private LeaveRepository leaveRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private LeaveDetailRepository leaveDetailRepository;

    @Override
    public ResponseDTO addLeaveDetails(LeaveDetailsDTO leaveDetailsDto) {

        double annualLeaveCount = leaveDetailsDto.getAnnualLeaveCount();
        double casualLeaveCount = leaveDetailsDto.getCasualLeaveCount();
        double medicalLeaveCount = leaveDetailsDto.getMedicalLeaveCount();
        Double halfDayStatus = leaveDetailsDto.getHalfDayStatus();
        Double workingMinutes = leaveDetailsDto.getWorkingMinutes();
        Long employeeId = leaveDetailsDto.getEmployeeId();
        Double shortInMinutes = leaveDetailsDto.getShortInMinutes();
        Optional<Employee> employeeById = employeeRepository.findById(employeeId);


        if (employeeById.isPresent()){
            Employee employee = employeeById.get();

            if (annualLeaveCount == 0) {
                return new ResponseDTO(
                        false,
                        "Please insert annual leave count"
                );
            } else if (casualLeaveCount == 0) {
                return new ResponseDTO(
                        false,
                        "Please insert casual leave count"
                );
            } else if (medicalLeaveCount == 0) {
                return new ResponseDTO(
                        false,
                        "Please insert medical leave count"
                );
            } else if (halfDayStatus == 0) {
                return new ResponseDTO(
                        false,
                        "Please insert half day leave status"
                );
            } else if (workingMinutes == 0) {
                return new ResponseDTO(
                        false,
                        "Please insert minimum working minutes count"
                );
            } else {

                try {
                    Leave_Details leaveDetails = new Leave_Details();

                    leaveDetails.setAnnualLeaveCount(annualLeaveCount);
                    leaveDetails.setCasualLeaveCount(casualLeaveCount);
                    leaveDetails.setMedicalLeaveCount(medicalLeaveCount);
                    leaveDetails.setHalfDayStatus(halfDayStatus);
                    leaveDetails.setWorkingMinutes(workingMinutes);
                    leaveDetails.setShortInMinutes(shortInMinutes);
                    leaveDetails.setEmployee(employee);

                    leaveDetailRepository.save(leaveDetails);
                    addDefaultLeaveByRegistration(employee, annualLeaveCount, casualLeaveCount, medicalLeaveCount);

                    LeaveDetailsDTO detailsDto = new LeaveDetailsDTO();

                    detailsDto.setLeaveDetailId(leaveDetails.getLeaveDetailId());
                    detailsDto.setAnnualLeaveCount(leaveDetails.getAnnualLeaveCount());
                    detailsDto.setCasualLeaveCount(leaveDetails.getCasualLeaveCount());
                    detailsDto.setMedicalLeaveCount(leaveDetails.getMedicalLeaveCount());
                    detailsDto.setHalfDayStatus(leaveDetails.getHalfDayStatus());
                    detailsDto.setWorkingMinutes(leaveDetails.getWorkingMinutes());
                    detailsDto.setShortInMinutes(leaveDetails.getShortInMinutes());
                    detailsDto.setEmployeeId(leaveDetails.getEmployee().getEmployeeId());

                    return new ResponseDTO(
                            true,
                            employee.getFullName() + "'s leave details saved are successfully!",
                            detailsDto
                    );
                } catch (Exception exception) {
                    return new ResponseDTO(
                            false,
                            "Leave detail update fail!"
                    );
                }
            }
        } else {
            return new ResponseDTO(
                    false,
                    "Employee cannot found!"
            );
        }
    }

    @Override
    public ResponseDTO getEmployerLeaveDetails(String value) {
        List<Leave_Details> leaveDetailsByEmployer = leaveDetailRepository.getLeave_DetailsByEmployee_EmployeeNumberContainingOrEmployee_NicNumberContainingOrEmployee_FullNameContaining(value, value, value);

        ArrayList<LeaveDetailsDTO> detailsDto = new ArrayList<>();

        if (!leaveDetailsByEmployer.isEmpty()) {
            for (Leave_Details leaveDetails : leaveDetailsByEmployer) {
                LeaveDetailsDTO leaveDetailsDto = new LeaveDetailsDTO();

                leaveDetailsDto.setWorkingMinutes(leaveDetails.getWorkingMinutes());
                leaveDetailsDto.setHalfDayStatus(leaveDetails.getHalfDayStatus());
                leaveDetailsDto.setAnnualLeaveCount(leaveDetails.getAnnualLeaveCount());
                leaveDetailsDto.setCasualLeaveCount(leaveDetails.getCasualLeaveCount());
                leaveDetailsDto.setMedicalLeaveCount(leaveDetails.getMedicalLeaveCount());
                leaveDetailsDto.setShortInMinutes(leaveDetails.getShortInMinutes());
                leaveDetailsDto.setInTime(leaveDetails.getInTime());
                leaveDetailsDto.setOutTime(leaveDetails.getOutTime());
                leaveDetailsDto.setEmployeeId(leaveDetails.getEmployee().getEmployeeId());

                detailsDto.add(leaveDetailsDto);
            }

            return new ResponseDTO(
                    true,
                    "Employee leave details are found!",
                    detailsDto
            );

        } else {
            return new ResponseDTO(
                    false,
                    "Employee cannot found to show leave details"
            );
        }
    }

    @Override
    public ResponseDTO getAllLeaveDetails() {
        List<Leave_Details> leaveDetails = leaveDetailRepository.findAll();

        ArrayList<LeaveDetailsDTO> leaveDetailsDto = new ArrayList<>();

        if (!leaveDetails.isEmpty()) {
            for (Leave_Details details: leaveDetails) {
                LeaveDetailsDTO detailsDto = new LeaveDetailsDTO();

                detailsDto.setEmployeeId(details.getEmployee().getEmployeeId());
                detailsDto.setOutTime(details.getOutTime());
                detailsDto.setInTime(details.getInTime());
                detailsDto.setMedicalLeaveCount(details.getMedicalLeaveCount());
                detailsDto.setCasualLeaveCount(details.getCasualLeaveCount());
                detailsDto.setAnnualLeaveCount(details.getAnnualLeaveCount());
                detailsDto.setWorkingMinutes(details.getWorkingMinutes());
                detailsDto.setShortInMinutes(details.getShortInMinutes());
                detailsDto.setHalfDayStatus(details.getHalfDayStatus());

                leaveDetailsDto.add(detailsDto);
            }
            return new ResponseDTO(
                    true,
                    "All leave details are, ",
                    leaveDetailsDto
            );
        } else {
            return new ResponseDTO(
                    false,
                    "No any leave details to show!"
            );
        }
    }

    public void addDefaultLeaveByRegistration(Employee _employee, Double _annual_leave_count, Double _casual_leave_count, Double _medical_leave_count) {
        try {
            Em_Leave leave = new Em_Leave();

            leave.setApprovedPerson(null);
            leave.setAssignedPerson(null);
            leave.setAssignedPersonApproval(false);
            leave.setBalancedAnnualLeave(_annual_leave_count);
            leave.setBalancedCasualLeave(_casual_leave_count);
            leave.setBalancedMedicalLeave(_medical_leave_count);
            leave.setFromDate(null);
            leave.setHalf(null);
            leave.setHalfCount(0);
            leave.setHalfDate(null);
            leave.setLeaveType(null);
            leave.setNoPayLeave(0.0);
            leave.setSpendAnnualLeave(0.0);
            leave.setSpendCasualLeave(0.0);
            leave.setSpendMedicalLeave(0.0);
            leave.setStatus(false);
            leave.setToDate(null);
            leave.setEmployee(_employee);
            leave.setShortDate(null);
            leave.setShortOutTime(null);

            leaveRepository.save(leave);

        } catch (Exception exception) {
            System.out.println("Cannot add default leave data : " + exception);
        }
    }
}
