package lk.code4x.hrsys.service.Impl;

import lk.code4x.hrsys.dto.AdministratorDTO;
import lk.code4x.hrsys.dto.ResponseDTO;
import lk.code4x.hrsys.model.Administrator;
import lk.code4x.hrsys.repo.AdministratorRepository;
import lk.code4x.hrsys.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 11:23 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Service
public class AdministratorServiceImpl implements AdministratorService {

    @Autowired
    private AdministratorRepository administratorRepository;

    @Override
    public ResponseDTO administratorLogin(String email, String password) {

        try {
            Administrator administratorsByEmail = administratorRepository.getAdministratorsByEmail(email);

            if (password.equalsIgnoreCase(administratorsByEmail.getPassword())) {

                AdministratorDTO administratorDTO = new AdministratorDTO();

                administratorDTO.setAdministrator_id(administratorsByEmail.getAdministrator_id());
                administratorDTO.setEmail(administratorsByEmail.getEmail());
                administratorDTO.setPassword(administratorsByEmail.getPassword());

                return new ResponseDTO(true, "Welcome to HRSYS", administratorDTO);
            } else {

                return new ResponseDTO(false, "Incorrect Password. Try again");
            }

        } catch (NullPointerException exception) {
                return new ResponseDTO(false, "Admin Cannot Found");
        }

    }
}
