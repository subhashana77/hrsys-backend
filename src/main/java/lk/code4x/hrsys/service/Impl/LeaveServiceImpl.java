package lk.code4x.hrsys.service.Impl;

import lk.code4x.hrsys.dto.AnnualLeaveDTO;
import lk.code4x.hrsys.dto.EmLeaveDTO;
import lk.code4x.hrsys.dto.ResponseDTO;
import lk.code4x.hrsys.model.Em_Leave;
import lk.code4x.hrsys.model.Employee;
import lk.code4x.hrsys.model.Leave_Details;
import lk.code4x.hrsys.repo.EmployeeRepository;
import lk.code4x.hrsys.repo.LeaveDetailRepository;
import lk.code4x.hrsys.repo.LeaveRepository;
import lk.code4x.hrsys.service.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author dilshan.r
 * @created 22/03/2022 - 9:25 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Service
public class LeaveServiceImpl implements LeaveService {

    @Autowired
    private LeaveRepository leaveRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private LeaveDetailRepository leaveDetailRepository;

    @Override
    public ResponseDTO leaveRequest(EmLeaveDTO eLeaveDTO) {

        Double lastBalancedAnnualLeaveCount = leaveRepository.findLastBalancedAnnualLeaveCount(eLeaveDTO.getEmployeeId());
        Double lastSpendAnnualLeaveCount = leaveRepository.findLastSpendAnnualLeaveCount(eLeaveDTO.getEmployeeId());
        Double lastBalancedCasualLeaveCount = leaveRepository.findLastBalancedCasualLeaveCount(eLeaveDTO.getEmployeeId());
        Double lastSpendCasualLeaveCount = leaveRepository.findLastSpendCasualLeaveCount(eLeaveDTO.getEmployeeId());
        Double lastBalancedMedicalLeaveCount = leaveRepository.findLastBalancedMedicalLeaveCount(eLeaveDTO.getEmployeeId());
        Double lastSpendMedicalLeaveCount = leaveRepository.findLastSpendMedicalLeaveCount(eLeaveDTO.getEmployeeId());
        Double lastNoPayLeaveCount = leaveRepository.findLastNoPayLeaveCount(eLeaveDTO.getEmployeeId());
        Double lastHalfDayLeaveCount = leaveRepository.findLastHalfDayLeaveCount(eLeaveDTO.getEmployeeId());

        String leaveType = eLeaveDTO.getLeaveType();
        String assignPerson = eLeaveDTO.getAssignedPerson();
        String approvedPerson = eLeaveDTO.getApprovedPerson();
        boolean assignedPersonApproval = eLeaveDTO.isAssignedPersonApproval();
        String fromDate = eLeaveDTO.getFromDate();
        String toDate = eLeaveDTO.getToDate();
        String halfDate = eLeaveDTO.getHalfDate();
        String half = eLeaveDTO.getHalf();
        double halfCount = eLeaveDTO.getHalfCount();
        Double balancedAnnualLeave = eLeaveDTO.getBalancedAnnualLeave();
        Double balancedCasualLeave = eLeaveDTO.getBalancedCasualLeave();
        Double balancedMedicalLeave = eLeaveDTO.getBalancedMedicalLeave();
        Double noPayLeave = eLeaveDTO.getNoPayLeave();
        Double spendAnnualLeave = eLeaveDTO.getSpendAnnualLeave();
        Double spendCasualLeave = eLeaveDTO.getSpendCasualLeave();
        Double spendMedicalLeave = eLeaveDTO.getSpendMedicalLeave();
        boolean status = eLeaveDTO.isStatus();
        String shortDate = eLeaveDTO.getShortDate();
        String shortOutTime = eLeaveDTO.getShortOutTime();
        Long employeeId = eLeaveDTO.getEmployeeId();

        Optional<Employee> employeeById = employeeRepository.findById(employeeId);
        String[] leave_types = {"ANNUAL", "CASUAL", "MEDICAL", "HALF_DAY", "SHORT_LEAVE", "NO_PAY"};
        SimpleDateFormat sdf = new SimpleDateFormat( "dd-MM-yyyy HH:mm:ss");
        long difference_In_Hours = 0;
        long difference_In_Days = 0;

        try {
            Date d1 = sdf.parse(fromDate);
            Date d2 = sdf.parse(toDate);

            long difference_In_Time = d2.getTime() - d1.getTime();

            difference_In_Hours = (difference_In_Time / (1000 * 60 * 60)) % 24;
            difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24)) % 365;

        } catch (Exception e) {
            return new ResponseDTO(
                    false,
                    "Should have insert leave from date and leave to date first"
            );
        }

        Optional<Leave_Details> leaveDetailRepositoryById = leaveDetailRepository.findById(employeeId);

        if (leaveDetailRepositoryById.isPresent()) {

            try {
                if (leaveType.equalsIgnoreCase(leave_types[0])) {

                    if (employeeById.isPresent()) {
                        Employee employee = employeeById.get();
                        if (assignPerson.isEmpty()) {
                            return new ResponseDTO(
                                    false,
                                    "You have to assign a person to cover your works first"
                            );
                        } else {
                            Em_Leave leave = new Em_Leave();
                            EmLeaveDTO leaveDTO = new EmLeaveDTO();

                            if (lastBalancedAnnualLeaveCount >= 1) {
                                try {
                                    leave.setLeaveType(leaveType);
                                    leave.setAssignedPerson(assignPerson);
                                    leave.setApprovedPerson(null);
                                    leave.setAssignedPersonApproval(false);
                                    leave.setFromDate(fromDate);
                                    leave.setToDate(toDate);
                                    leave.setHalfDate(null);
                                    leave.setHalf(null);
                                    leave.setHalfCount(0);
                                    leave.setBalancedAnnualLeave(lastBalancedAnnualLeaveCount - difference_In_Days);
                                    leave.setBalancedCasualLeave(lastBalancedCasualLeaveCount);
                                    leave.setBalancedMedicalLeave(lastBalancedMedicalLeaveCount);
                                    leave.setNoPayLeave(lastNoPayLeaveCount);
                                    leave.setSpendAnnualLeave(lastSpendAnnualLeaveCount + difference_In_Days);
                                    leave.setSpendCasualLeave(lastSpendCasualLeaveCount);
                                    leave.setSpendMedicalLeave(lastSpendMedicalLeaveCount);
                                    leave.setStatus(false);
                                    leave.setShortDate(null);
                                    leave.setShortOutTime(null);
                                    leave.setEmployee(employee);

                                    leaveRepository.save(leave);

                                    leaveDTO.setLeaveType(leave.getLeaveType());
                                    leaveDTO.setAssignedPerson(leave.getAssignedPerson());
                                    leaveDTO.setApprovedPerson(leave.getApprovedPerson());
                                    leaveDTO.setAssignedPersonApproval(leave.isAssignedPersonApproval());
                                    leaveDTO.setFromDate(leave.getFromDate());
                                    leaveDTO.setToDate(leave.getToDate());
                                    leaveDTO.setHalfDate(leave.getHalfDate());
                                    leaveDTO.setHalf(leave.getHalf());
                                    leaveDTO.setHalfCount(leave.getHalfCount());
                                    leaveDTO.setBalancedAnnualLeave(leave.getBalancedAnnualLeave());
                                    leaveDTO.setBalancedCasualLeave(leave.getBalancedCasualLeave());
                                    leaveDTO.setBalancedMedicalLeave(leave.getBalancedMedicalLeave());
                                    leaveDTO.setNoPayLeave(leave.getNoPayLeave());
                                    leaveDTO.setSpendAnnualLeave(leave.getSpendAnnualLeave());
                                    leaveDTO.setSpendCasualLeave(leave.getSpendCasualLeave());
                                    leaveDTO.setSpendMedicalLeave(leave.getSpendMedicalLeave());
                                    leaveDTO.setStatus(leave.isStatus());
                                    leaveDTO.setShortDate(leave.getShortDate());
                                    leaveDTO.setShortOutTime(leave.getShortOutTime());
                                    leaveDTO.setEmployeeId(leave.getEmployee().getEmployeeId());

                                    return new ResponseDTO(
                                            true,
                                            "Your annual leave request has sent",
                                            leaveDTO
                                    );

                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                    return new ResponseDTO(
                                            false,
                                            "Annual Leave request fail!",
                                            601
                                    );
                                }

                            }
                        }
                    } else {
                        return new ResponseDTO(
                                false,
                                "Cannot find the employer"
                        );
                    }
                    return new ResponseDTO(
                            false,
                            "Something went Wrong! Please try again."
                    );

                } else if (leaveType.equalsIgnoreCase(leave_types[1])) {

                    if (employeeById.isPresent()) {
                        Employee employee = employeeById.get();
                        if (assignPerson.isEmpty()) {
                            return new ResponseDTO(
                                    false,
                                    "You have to assign a person to cover your works first"
                            );
                        } else {
                            Em_Leave leave = new Em_Leave();
                            EmLeaveDTO leaveDTO = new EmLeaveDTO();

                            if (lastBalancedCasualLeaveCount >= 1) {
                                try {
                                    leave.setLeaveType(leaveType);
                                    leave.setAssignedPerson(assignPerson);
                                    leave.setApprovedPerson(null);
                                    leave.setAssignedPersonApproval(false);
                                    leave.setFromDate(fromDate);
                                    leave.setToDate(toDate);
                                    leave.setHalfDate(null);
                                    leave.setHalf(null);
                                    leave.setHalfCount(0);
                                    leave.setBalancedAnnualLeave(lastBalancedAnnualLeaveCount);
                                    leave.setBalancedCasualLeave(lastBalancedCasualLeaveCount - difference_In_Days);
                                    leave.setBalancedMedicalLeave(lastBalancedMedicalLeaveCount);
                                    leave.setNoPayLeave(lastNoPayLeaveCount);
                                    leave.setSpendAnnualLeave(lastSpendAnnualLeaveCount);
                                    leave.setSpendCasualLeave(lastSpendCasualLeaveCount + difference_In_Days);
                                    leave.setSpendMedicalLeave(lastSpendMedicalLeaveCount);
                                    leave.setStatus(false);
                                    leave.setShortDate(null);
                                    leave.setShortOutTime(null);
                                    leave.setEmployee(employee);

                                    leaveRepository.save(leave);

                                    leaveDTO.setLeaveType(leave.getLeaveType());
                                    leaveDTO.setAssignedPerson(leave.getAssignedPerson());
                                    leaveDTO.setApprovedPerson(leave.getApprovedPerson());
                                    leaveDTO.setAssignedPersonApproval(leave.isAssignedPersonApproval());
                                    leaveDTO.setFromDate(leave.getFromDate());
                                    leaveDTO.setToDate(leave.getToDate());
                                    leaveDTO.setHalfDate(leave.getHalfDate());
                                    leaveDTO.setHalf(leave.getHalf());
                                    leaveDTO.setHalfCount(leave.getHalfCount());
                                    leaveDTO.setBalancedAnnualLeave(leave.getBalancedAnnualLeave());
                                    leaveDTO.setBalancedCasualLeave(leave.getBalancedCasualLeave());
                                    leaveDTO.setBalancedMedicalLeave(leave.getBalancedMedicalLeave());
                                    leaveDTO.setNoPayLeave(leave.getNoPayLeave());
                                    leaveDTO.setSpendAnnualLeave(leave.getSpendAnnualLeave());
                                    leaveDTO.setSpendCasualLeave(leave.getSpendCasualLeave());
                                    leaveDTO.setSpendMedicalLeave(leave.getSpendMedicalLeave());
                                    leaveDTO.setStatus(leave.isStatus());
                                    leaveDTO.setShortDate(leave.getShortDate());
                                    leaveDTO.setShortOutTime(leave.getShortOutTime());
                                    leaveDTO.setEmployeeId(leave.getEmployee().getEmployeeId());

                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                    return new ResponseDTO(
                                            false,
                                            "Annual Leave request fail!",
                                            601
                                    );
                                }
                                return new ResponseDTO(
                                        true,
                                        "Your casual leave request has sent",
                                        leaveDTO
                                );
                            }
                        }
                    } else {
                        return new ResponseDTO(
                                false,
                                "Cannot find the employer"
                        );
                    }
                    return new ResponseDTO(
                            false,
                            "Something went Wrong! Please try again."
                    );
                } else if (leaveType.equalsIgnoreCase(leave_types[2])) {

                    if (employeeById.isPresent()) {
                        Employee employee = employeeById.get();
                        if (assignPerson.isEmpty()) {
                            return new ResponseDTO(
                                    false,
                                    "You have to assign a person to cover your works first"
                            );
                        } else {
                            Em_Leave leave = new Em_Leave();
                            EmLeaveDTO leaveDTO = new EmLeaveDTO();

                            if (lastBalancedMedicalLeaveCount >= 1) {
                                try {
                                    leave.setLeaveType(leaveType);
                                    leave.setAssignedPerson(assignPerson);
                                    leave.setApprovedPerson(null);
                                    leave.setAssignedPersonApproval(false);
                                    leave.setFromDate(fromDate);
                                    leave.setToDate(toDate);
                                    leave.setHalfDate(null);
                                    leave.setHalf(null);
                                    leave.setHalfCount(0);
                                    leave.setBalancedAnnualLeave(lastBalancedAnnualLeaveCount);
                                    leave.setBalancedCasualLeave(lastBalancedCasualLeaveCount);
                                    leave.setBalancedMedicalLeave(lastBalancedMedicalLeaveCount - difference_In_Days);
                                    leave.setNoPayLeave(lastNoPayLeaveCount);
                                    leave.setSpendAnnualLeave(lastSpendAnnualLeaveCount);
                                    leave.setSpendCasualLeave(lastSpendCasualLeaveCount);
                                    leave.setSpendMedicalLeave(lastSpendMedicalLeaveCount + difference_In_Days);
                                    leave.setStatus(false);
                                    leave.setShortDate(null);
                                    leave.setShortOutTime(null);
                                    leave.setEmployee(employee);

                                    leaveRepository.save(leave);

                                    leaveDTO.setLeaveType(leave.getLeaveType());
                                    leaveDTO.setAssignedPerson(leave.getAssignedPerson());
                                    leaveDTO.setApprovedPerson(leave.getApprovedPerson());
                                    leaveDTO.setAssignedPersonApproval(leave.isAssignedPersonApproval());
                                    leaveDTO.setFromDate(leave.getFromDate());
                                    leaveDTO.setToDate(leave.getToDate());
                                    leaveDTO.setHalfDate(leave.getHalfDate());
                                    leaveDTO.setHalf(leave.getHalf());
                                    leaveDTO.setHalfCount(leave.getHalfCount());
                                    leaveDTO.setBalancedAnnualLeave(leave.getBalancedAnnualLeave());
                                    leaveDTO.setBalancedCasualLeave(leave.getBalancedCasualLeave());
                                    leaveDTO.setBalancedMedicalLeave(leave.getBalancedMedicalLeave());
                                    leaveDTO.setNoPayLeave(leave.getNoPayLeave());
                                    leaveDTO.setSpendAnnualLeave(leave.getSpendAnnualLeave());
                                    leaveDTO.setSpendCasualLeave(leave.getSpendCasualLeave());
                                    leaveDTO.setSpendMedicalLeave(leave.getSpendMedicalLeave());
                                    leaveDTO.setStatus(leave.isStatus());
                                    leaveDTO.setShortDate(leave.getShortDate());
                                    leaveDTO.setShortOutTime(leave.getShortOutTime());
                                    leaveDTO.setEmployeeId(leave.getEmployee().getEmployeeId());

                                    return new ResponseDTO(
                                            true,
                                            "Your medical leave request has sent",
                                            leaveDTO
                                    );

                                } catch (Exception exception) {
                                    return new ResponseDTO(
                                            false,
                                            "Medical Leave request fail!",
                                            601
                                    );
                                }
                            }
                        }
                    } else {
                        return new ResponseDTO(
                                false,
                                "Cannot find the employer"
                        );
                    }
                    return new ResponseDTO(
                            false,
                            "Something went Wrong! Please try again."
                    );
                } else if (leaveType.equalsIgnoreCase(leave_types[3])) {

                    if (employeeById.isPresent()) {
                        Employee employee = employeeById.get();
                        if (assignPerson.isEmpty()) {
                            return new ResponseDTO(
                                    false,
                                    "You have to assign a person to cover your works first"
                            );
                        } else {
                            try {
                                Em_Leave leave = new Em_Leave();
                                EmLeaveDTO leaveDTO = new EmLeaveDTO();

                                leave.setLeaveType(leaveType);
                                leave.setAssignedPerson(assignPerson);
                                leave.setApprovedPerson(null);
                                leave.setAssignedPersonApproval(false);
                                leave.setFromDate(null);
                                leave.setToDate(null);
                                leave.setHalfDate(halfDate);
                                leave.setHalf(half);
                                leave.setHalfCount(lastHalfDayLeaveCount + 0.5);
                                leave.setBalancedAnnualLeave(lastBalancedAnnualLeaveCount);
                                leave.setBalancedCasualLeave(lastBalancedCasualLeaveCount);
                                leave.setBalancedMedicalLeave(lastBalancedMedicalLeaveCount);
                                leave.setNoPayLeave(lastNoPayLeaveCount);
                                leave.setSpendAnnualLeave(lastSpendAnnualLeaveCount);
                                leave.setSpendCasualLeave(lastSpendCasualLeaveCount);
                                leave.setSpendMedicalLeave(lastSpendMedicalLeaveCount);
                                leave.setStatus(false);
                                leave.setShortDate(null);
                                leave.setShortOutTime(null);
                                leave.setEmployee(employee);

                                leaveRepository.save(leave);

                                leaveDTO.setLeaveType(leave.getLeaveType());
                                leaveDTO.setAssignedPerson(leave.getAssignedPerson());
                                leaveDTO.setApprovedPerson(leave.getApprovedPerson());
                                leaveDTO.setAssignedPersonApproval(leave.isAssignedPersonApproval());
                                leaveDTO.setFromDate(leave.getFromDate());
                                leaveDTO.setToDate(leave.getToDate());
                                leaveDTO.setHalfDate(leave.getHalfDate());
                                leaveDTO.setHalf(leave.getHalf());
                                leaveDTO.setHalfCount(leave.getHalfCount());
                                leaveDTO.setBalancedAnnualLeave(leave.getBalancedAnnualLeave());
                                leaveDTO.setBalancedCasualLeave(leave.getBalancedCasualLeave());
                                leaveDTO.setBalancedMedicalLeave(leave.getBalancedMedicalLeave());
                                leaveDTO.setNoPayLeave(leave.getNoPayLeave());
                                leaveDTO.setSpendAnnualLeave(leave.getSpendAnnualLeave());
                                leaveDTO.setSpendCasualLeave(leave.getSpendCasualLeave());
                                leaveDTO.setSpendMedicalLeave(leave.getSpendMedicalLeave());
                                leaveDTO.setStatus(leave.isStatus());
                                leaveDTO.setShortDate(leave.getShortDate());
                                leaveDTO.setShortOutTime(leave.getShortOutTime());
                                leaveDTO.setEmployeeId(leave.getEmployee().getEmployeeId());

                                return new ResponseDTO(
                                        true,
                                        "Your half-day leave request has sent",
                                        leaveDTO
                                );
                            } catch (Exception exception) {
                                return new ResponseDTO(
                                        false,
                                        "Half-day leave request fail!",
                                        601
                                );
                            }
                        }
                    } else {
                        return new ResponseDTO(
                                false,
                                "Cannot find the employer"
                        );
                    }
                } else if (leaveType.equalsIgnoreCase(leave_types[4])) {

                    if (employeeById.isPresent()) {
                        Employee employee = employeeById.get();
                        if (assignPerson.isEmpty()) {
                            return new ResponseDTO(
                                    false,
                                    "You have to assign a person to cover your works first"
                            );
                        } else {
                            Em_Leave leave = new Em_Leave();
                            EmLeaveDTO leaveDTO = new EmLeaveDTO();

                            try {
                                leave.setLeaveType(leaveType);
                                leave.setAssignedPerson(assignPerson);
                                leave.setApprovedPerson(null);
                                leave.setAssignedPersonApproval(false);
                                leave.setFromDate(null);
                                leave.setToDate(null);
                                leave.setHalfDate(null);
                                leave.setHalf(null);
                                leave.setHalfCount(0);
                                leave.setBalancedAnnualLeave(lastBalancedAnnualLeaveCount);
                                leave.setBalancedCasualLeave(lastBalancedCasualLeaveCount);
                                leave.setBalancedMedicalLeave(lastBalancedMedicalLeaveCount);
                                leave.setNoPayLeave(lastNoPayLeaveCount);
                                leave.setSpendAnnualLeave(lastSpendAnnualLeaveCount);
                                leave.setSpendCasualLeave(lastSpendCasualLeaveCount);
                                leave.setSpendMedicalLeave(lastSpendMedicalLeaveCount);
                                leave.setStatus(false);
                                leave.setShortDate(shortDate);
                                leave.setShortOutTime(shortOutTime);
                                leave.setEmployee(employee);

                                leaveRepository.save(leave);

                                leaveDTO.setLeaveType(leave.getLeaveType());
                                leaveDTO.setAssignedPerson(leave.getAssignedPerson());
                                leaveDTO.setApprovedPerson(leave.getApprovedPerson());
                                leaveDTO.setAssignedPersonApproval(leave.isAssignedPersonApproval());
                                leaveDTO.setFromDate(leave.getFromDate());
                                leaveDTO.setToDate(leave.getToDate());
                                leaveDTO.setHalfDate(leave.getHalfDate());
                                leaveDTO.setHalf(leave.getHalf());
                                leaveDTO.setHalfCount(leave.getHalfCount());
                                leaveDTO.setBalancedAnnualLeave(leave.getBalancedAnnualLeave());
                                leaveDTO.setBalancedCasualLeave(leave.getBalancedCasualLeave());
                                leaveDTO.setBalancedMedicalLeave(leave.getBalancedMedicalLeave());
                                leaveDTO.setNoPayLeave(leave.getNoPayLeave());
                                leaveDTO.setSpendAnnualLeave(leave.getSpendAnnualLeave());
                                leaveDTO.setSpendCasualLeave(leave.getSpendCasualLeave());
                                leaveDTO.setSpendMedicalLeave(leave.getSpendMedicalLeave());
                                leaveDTO.setStatus(leave.isStatus());
                                leaveDTO.setShortDate(leave.getShortDate());
                                leaveDTO.setShortOutTime(leave.getShortOutTime());
                                leaveDTO.setEmployeeId(leave.getEmployee().getEmployeeId());

                                return new ResponseDTO(
                                        true,
                                        "Your No-Pay leave request has sent",
                                        leaveDTO
                                );

                            } catch (Exception exception) {
                                exception.printStackTrace();
                                return new ResponseDTO(
                                        false,
                                        "No-Pay Leave request fail!",
                                        601
                                );
                            }
                        }
                    } else {
                        return new ResponseDTO(
                                false,
                                "Cannot find the employer"
                        );
                    }
                } else if (leaveType.equalsIgnoreCase(leave_types[5])) {
                    if (employeeById.isPresent()) {
                        Employee employee = employeeById.get();

                        if (assignPerson.isEmpty()) {
                            return new ResponseDTO(
                                    false,
                                    "You have to assign a person to cover your works first"
                            );
                        }  else {
                            Em_Leave leave = new Em_Leave();
                            EmLeaveDTO leaveDTO = new EmLeaveDTO();

                            try {
                                leave.setLeaveType(leaveType);
                                leave.setAssignedPerson(assignPerson);
                                leave.setApprovedPerson(null);
                                leave.setAssignedPersonApproval(false);
                                leave.setFromDate(fromDate);
                                leave.setToDate(toDate);
                                leave.setHalfDate(null);
                                leave.setHalf(null);
                                leave.setHalfCount(0);
                                leave.setBalancedAnnualLeave(lastBalancedAnnualLeaveCount);
                                leave.setBalancedCasualLeave(lastBalancedCasualLeaveCount);
                                leave.setBalancedMedicalLeave(lastBalancedMedicalLeaveCount);
                                leave.setNoPayLeave(lastNoPayLeaveCount);
                                leave.setSpendAnnualLeave(lastSpendAnnualLeaveCount);
                                leave.setSpendCasualLeave(lastSpendCasualLeaveCount);
                                leave.setSpendMedicalLeave(lastSpendMedicalLeaveCount);
                                leave.setStatus(false);
                                leave.setShortDate(shortDate);
                                leave.setShortOutTime(shortOutTime);
                                leave.setEmployee(employee);

                                leaveRepository.save(leave);

                                leaveDTO.setLeaveType(leave.getLeaveType());
                                leaveDTO.setAssignedPerson(leave.getAssignedPerson());
                                leaveDTO.setApprovedPerson(leave.getApprovedPerson());
                                leaveDTO.setAssignedPersonApproval(leave.isAssignedPersonApproval());
                                leaveDTO.setFromDate(leave.getFromDate());
                                leaveDTO.setToDate(leave.getToDate());
                                leaveDTO.setHalfDate(leave.getHalfDate());
                                leaveDTO.setHalf(leave.getHalf());
                                leaveDTO.setHalfCount(leave.getHalfCount());
                                leaveDTO.setBalancedAnnualLeave(leave.getBalancedAnnualLeave());
                                leaveDTO.setBalancedCasualLeave(leave.getBalancedCasualLeave());
                                leaveDTO.setBalancedMedicalLeave(leave.getBalancedMedicalLeave());
                                leaveDTO.setNoPayLeave(leave.getNoPayLeave());
                                leaveDTO.setSpendAnnualLeave(leave.getSpendAnnualLeave());
                                leaveDTO.setSpendCasualLeave(leave.getSpendCasualLeave());
                                leaveDTO.setSpendMedicalLeave(leave.getSpendMedicalLeave());
                                leaveDTO.setStatus(leave.isStatus());
                                leaveDTO.setShortDate(leave.getShortDate());
                                leaveDTO.setShortOutTime(leave.getShortOutTime());
                                leaveDTO.setEmployeeId(leave.getEmployee().getEmployeeId());

                                return new ResponseDTO(
                                        true,
                                        "Your No-Pay leave request has sent",
                                        leaveDTO
                                );

                            } catch (Exception exception) {
                                exception.printStackTrace();
                                return new ResponseDTO(
                                        false,
                                        "No Pay Leave request fail!",
                                        601
                                );
                            }
                        }
                    } else {
                        return new ResponseDTO(
                                false,
                                "Cannot find the employer"
                        );
                    }
                } else {
                    return new ResponseDTO(
                            false,
                            "Please Select proper leave type"
                    );
                }
            } catch (Exception exception) {
                return new ResponseDTO(
                        false,
                        "Something went Wrong! Please try again."
                );
            }
        } else {
            return new ResponseDTO(
                    false,
                    "Cannot find the employer"
            );
        }
    }

    @Override
    public ResponseDTO getAllLeaves(Long value) {
        List<Em_Leave> emLeaveList = leaveRepository.getEm_LeaveByEmployee_EmployeeId(value);

        if (!emLeaveList.isEmpty()) {
            ArrayList<EmLeaveDTO> emLeaveDTOS = new ArrayList<>();

            for (Em_Leave emLeave: emLeaveList) {
                EmLeaveDTO leaveDTO = new EmLeaveDTO();

                leaveDTO.setShortOutTime(emLeave.getShortOutTime());
                leaveDTO.setShortDate(emLeave.getShortOutTime());
                leaveDTO.setBalancedAnnualLeave(emLeave.getBalancedAnnualLeave());
                leaveDTO.setEmployeeId(emLeave.getEmployee().getEmployeeId());
                leaveDTO.setApprovedPerson(emLeave.getApprovedPerson());
                leaveDTO.setAssignedPerson(emLeave.getAssignedPerson());
                leaveDTO.setAssignedPersonApproval(emLeave.isAssignedPersonApproval());
                leaveDTO.setBalancedCasualLeave(emLeave.getBalancedCasualLeave());
                leaveDTO.setBalancedMedicalLeave(emLeave.getBalancedMedicalLeave());
                leaveDTO.setBalancedAnnualLeave(emLeave.getBalancedAnnualLeave());
                leaveDTO.setFromDate(emLeave.getToDate());
                leaveDTO.setHalf(emLeave.getHalf());
                leaveDTO.setHalfCount(emLeave.getHalfCount());
                leaveDTO.setHalfDate(emLeave.getHalfDate());
                leaveDTO.setLeaveType(emLeave.getLeaveType());
                leaveDTO.setNoPayLeave(emLeave.getNoPayLeave());
                leaveDTO.setSpendAnnualLeave(emLeave.getSpendAnnualLeave());
                leaveDTO.setSpendCasualLeave(emLeave.getSpendCasualLeave());
                leaveDTO.setSpendMedicalLeave(emLeave.getSpendMedicalLeave());
                leaveDTO.setStatus(emLeave.isStatus());
                leaveDTO.setToDate(emLeave.getToDate());
                leaveDTO.setLeaveId(emLeave.getLeaveId());

                emLeaveDTOS.add(leaveDTO);
            }

            return new ResponseDTO(
                    true,
                    "All employee leave details are, ",
                    emLeaveDTOS
            );
        } else {
            return new ResponseDTO(
                    false,
                    "Cannot find any leaves"
            );
        }
    }

    @Override
    public ResponseDTO getAnnualLeave(Long value) {
        List<Em_Leave> annualLeave = leaveRepository.getAnnualLeave(value);

        if (!annualLeave.isEmpty()) {
            ArrayList<AnnualLeaveDTO> annualLeaveDTOS = new ArrayList<>();

            for (Em_Leave emLeave: annualLeave) {
                AnnualLeaveDTO annualLeaveDTO = new AnnualLeaveDTO();

                annualLeaveDTO.setApprovedPerson(emLeave.getApprovedPerson());
                annualLeaveDTO.setAssignPerson(emLeave.getApprovedPerson());
                annualLeaveDTO.setBalancedAnnualLeave(emLeave.getBalancedMedicalLeave());
                annualLeaveDTO.setEmployeeId(emLeave.getEmployee().getEmployeeId());
                annualLeaveDTO.setLeaveFromDate(emLeave.getFromDate());
                annualLeaveDTO.setLeaveId(emLeave.getLeaveId());
                annualLeaveDTO.setLeaveToDate(emLeave.getToDate());
                annualLeaveDTO.setSpendAnnualLeave(emLeave.getSpendAnnualLeave());
                annualLeaveDTO.setEmployeeEmail(emLeave.getEmployee().getEmail());
                annualLeaveDTO.setEmployeeName(emLeave.getEmployee().getFullName());
                annualLeaveDTO.setEmployeeNic(emLeave.getEmployee().getNicNumber());
                annualLeaveDTO.setEmployeeNumber(emLeave.getEmployee().getEmployeeNumber());

                annualLeaveDTOS.add(annualLeaveDTO);
            }

            return new ResponseDTO(
                    true,
                    "All annual leaves are",
                    annualLeaveDTOS
            );
        } else {
            return new ResponseDTO(
                    false,
                    "Cannot find any annual leave"
            );
        }
    }

    @Override
    public ResponseDTO removeLeave(Long id) {
        Optional<Em_Leave> leaveById = leaveRepository.findById(id);
        if (leaveById.isPresent()) {
            Em_Leave em_leave = leaveById.get();
            leaveRepository.delete(em_leave);
            return new ResponseDTO(
                    true,
                    "Leave delete successfully"
            );
        } else {
            return new ResponseDTO(
                    false,
                    "Delete fail, cannot find this leave request"
            );
        }
    }
}
