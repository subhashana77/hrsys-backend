package lk.code4x.hrsys.service;

import lk.code4x.hrsys.dto.EmLeaveDTO;
import lk.code4x.hrsys.dto.ResponseDTO;

/**
 * @author dilshan.r
 * @created 22/03/2022 - 9:25 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public interface LeaveService {
    public ResponseDTO leaveRequest(EmLeaveDTO eLeaveDTO);
    public ResponseDTO getAllLeaves(Long value);
    public ResponseDTO getAnnualLeave(Long value);
    public ResponseDTO removeLeave(Long id);
}
