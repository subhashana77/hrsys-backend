package lk.code4x.hrsys.service;

import lk.code4x.hrsys.dto.EmployeeDTO;
import lk.code4x.hrsys.dto.ResponseDTO;
import lk.code4x.hrsys.model.Employee;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 11:20 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public interface AdministratorService {
    public ResponseDTO administratorLogin(String email, String password);
}
