package lk.code4x.hrsys.service;

import lk.code4x.hrsys.dto.LeaveDetailsDTO;
import lk.code4x.hrsys.dto.ResponseDTO;

/**
 * @author dilshan.r
 * @created 30/03/2022 - 12:02 PM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public interface LeaveDetailService {
    public ResponseDTO addLeaveDetails(LeaveDetailsDTO leaveDetailsDto);
    public ResponseDTO getEmployerLeaveDetails(String value);
    public ResponseDTO getAllLeaveDetails();
}
