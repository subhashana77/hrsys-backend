package lk.code4x.hrsys.service;

import lk.code4x.hrsys.dto.EmployeeDTO;
import lk.code4x.hrsys.dto.ResponseDTO;

/**
 * @author dilshan.r
 * @created 18/03/2022 - 9:25 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public interface EmployeeService {
    public ResponseDTO employeeRegistration(EmployeeDTO employeeDTO);
    public ResponseDTO employeeUpdate(EmployeeDTO employeeDTO, Long employeeId);
    public ResponseDTO employeeDelete(Long employeeId);
    public ResponseDTO employeeGetAll();
    public ResponseDTO employeeSearch(String keyword);
}
