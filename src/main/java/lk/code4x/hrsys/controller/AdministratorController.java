package lk.code4x.hrsys.controller;

import lk.code4x.hrsys.dto.AdministratorDTO;
import lk.code4x.hrsys.dto.EmployeeDTO;
import lk.code4x.hrsys.dto.LeaveDetailsDTO;
import lk.code4x.hrsys.dto.ResponseDTO;
import lk.code4x.hrsys.service.AdministratorService;
import lk.code4x.hrsys.service.EmployeeService;
import lk.code4x.hrsys.service.LeaveDetailService;
import lk.code4x.hrsys.service.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 11:04 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@RestController
@CrossOrigin
@RequestMapping("/api/v1.0/admin")
public class AdministratorController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private LeaveService leaveService;

    @Autowired
    private LeaveDetailService leaveDetailService;

    @PostMapping("/login")
    public ResponseDTO administratorLogin(@RequestBody AdministratorDTO administratorDTO) {
        return administratorService.administratorLogin(administratorDTO.getEmail(), administratorDTO.getPassword());
    }

    @PostMapping("/registration")
    public ResponseDTO employeeRegistration(@RequestBody EmployeeDTO employeeDTO) {
        return employeeService.employeeRegistration(employeeDTO);
    }

    @PostMapping("/leave-details")
    public ResponseDTO addLeaveDetails(@RequestBody LeaveDetailsDTO leaveDetailsDto) {
        return leaveDetailService.addLeaveDetails(leaveDetailsDto);
    }

    @PutMapping("/update")
    public ResponseDTO employeeUpdate(@RequestBody EmployeeDTO employeeDTO, @RequestParam("employeeId") Long employeeId) {
        return employeeService.employeeUpdate(employeeDTO, employeeId);
    }

    @DeleteMapping("/delete")
    public ResponseDTO employeeDelete(@RequestParam("employeeId") Long employeeId) {
        return employeeService.employeeDelete(employeeId);
    }

    @GetMapping("/all-employee")
    public ResponseDTO employeeGetAll() {
        return employeeService.employeeGetAll();
    }

    @GetMapping("/employee-search")
    public ResponseDTO employeeSearch(@RequestParam("keyword") String keyword) {
        return employeeService.employeeSearch(keyword);
    }

    @GetMapping("/search-leave-details")
    public ResponseDTO getEmployerLeaveDetails(@RequestParam("value") String value) {
        return leaveDetailService.getEmployerLeaveDetails(value);
    }

    @GetMapping("/all-leave-details")
    public ResponseDTO getAllLeaveDetails() {
        return leaveDetailService.getAllLeaveDetails();
    }

    @GetMapping("/all-leaves")
    public ResponseDTO getAllLeaves(@RequestParam("value") Long value) {
        return leaveService.getAllLeaves(value);
    }

    @GetMapping("/find-annual-leaves")
    public ResponseDTO getAnnualLeave(@RequestParam("value") Long value) {
        return leaveService.getAnnualLeave(value);
    }

}
