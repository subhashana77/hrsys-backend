package lk.code4x.hrsys.controller;

import lk.code4x.hrsys.dto.EmLeaveDTO;
import lk.code4x.hrsys.dto.ResponseDTO;
import lk.code4x.hrsys.service.LeaveDetailService;
import lk.code4x.hrsys.service.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author dilshan.r
 * @created 22/03/2022 - 9:18 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@RestController
@CrossOrigin
@RequestMapping("/api/v1.0/employee")
public class EmployeeController {

    @Autowired
    private LeaveService leaveService;

    @Autowired
    private LeaveDetailService leaveDetailService;

    @PostMapping("/request")
    public ResponseDTO leaveRequest(@RequestBody EmLeaveDTO eLeaveDTO){
        return leaveService.leaveRequest(eLeaveDTO);
    }

    @GetMapping("/search-leave-details")
    public ResponseDTO getEmployerLeaveDetails(@RequestParam("value") String value) {
        return leaveDetailService.getEmployerLeaveDetails(value);
    }

    @DeleteMapping("/delete")
    public ResponseDTO removeLeave(@RequestParam("leaveId") Long leaveId) {
        return leaveService.removeLeave(leaveId);
    }
}
