package lk.code4x.hrsys.repo;

import lk.code4x.hrsys.model.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 10:59 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Long> {
    public Administrator getAdministratorsByEmail(String email);
}
