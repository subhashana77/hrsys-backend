package lk.code4x.hrsys.repo;

import lk.code4x.hrsys.model.Leave_Details;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author dilshan.r
 * @created 23/03/2022 - 11:50 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Repository
public interface LeaveDetailRepository extends JpaRepository<Leave_Details, Long> {
    public List<Leave_Details> getLeave_DetailsByEmployee_EmployeeNumberContainingOrEmployee_NicNumberContainingOrEmployee_FullNameContaining(String employee_employeeNumber, String employee_nicNumber, String employee_fullName);
}
