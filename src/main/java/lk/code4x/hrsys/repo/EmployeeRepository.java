package lk.code4x.hrsys.repo;

import lk.code4x.hrsys.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author dilshan.r
 * @created 18/03/2022 - 9:33 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    public Employee getEmployeeByEmail(String email);

    public Employee getEmployeeByNicNumber(String nicNumber);

    @Query("FROM Employee e WHERE e.fullName LIKE %:keyword% OR e.employeeNumber LIKE %:keyword% OR e.nicNumber LIKE %:keyword%")
    public List<Employee> searchEmployee(@Param("keyword") String keyword);

}
