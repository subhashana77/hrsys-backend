package lk.code4x.hrsys.repo;

import lk.code4x.hrsys.model.Em_Leave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author dilshan.r
 * @created 22/03/2022 - 9:52 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Repository
public interface LeaveRepository extends JpaRepository<Em_Leave, Long> {

    @Query( value =
            "SELECT balanced_annual_leave " +
                    "FROM em_leave " +
                    "WHERE employee_id = ?1 " +
                    "ORDER BY balanced_annual_leave " +
                    "ASC LIMIT 1",
            nativeQuery = true
    )
    public Double findLastBalancedAnnualLeaveCount(Long employee_id);

    @Query( value =
            "SELECT spend_annual_leave " +
                    "FROM em_leave " +
                    "WHERE employee_id = ?1 " +
                    "ORDER BY spend_annual_leave " +
                    "DESC LIMIT 1",
            nativeQuery = true
    )
    public Double findLastSpendAnnualLeaveCount(Long employee_id);

    @Query( value =
            "SELECT balanced_casual_leave " +
                    "FROM em_leave " +
                    "WHERE employee_id = ?1 " +
                    "ORDER BY balanced_casual_leave " +
                    "ASC LIMIT 1",
            nativeQuery = true
    )
    public Double findLastBalancedCasualLeaveCount(Long employee_id);

    @Query( value =
            "SELECT spend_casual_leave " +
                    "FROM em_leave " +
                    "WHERE employee_id = ?1 " +
                    "ORDER BY spend_casual_leave " +
                    "ASC LIMIT 1",
            nativeQuery = true
    )
    public Double findLastSpendCasualLeaveCount(Long employee_id);

    @Query( value =
            "SELECT balanced_medical_leave " +
                    "FROM em_leave " +
                    "WHERE employee_id = ?1 " +
                    "ORDER BY balanced_medical_leave " +
                    "ASC LIMIT 1",
            nativeQuery = true
    )
    public Double findLastBalancedMedicalLeaveCount(Long employee_id);

    @Query( value =
            "SELECT spend_medical_leave " +
                    "FROM em_leave " +
                    "WHERE employee_id = ?1 " +
                    "ORDER BY spend_medical_leave " +
                    "ASC LIMIT 1",
            nativeQuery = true
    )
    public Double findLastSpendMedicalLeaveCount(Long employee_id);

    @Query( value =
            "SELECT no_pay_leave " +
                    "FROM em_leave " +
                    "WHERE employee_id = ?1 " +
                    "ORDER BY no_pay_leave " +
                    "ASC LIMIT 1",
            nativeQuery = true
    )
    public Double findLastNoPayLeaveCount(Long employee_id);

    @Query( value =
            "SELECT half_count " +
                    "FROM em_leave " +
                    "WHERE employee_id = ?1 " +
                    "ORDER BY half_count " +
                    "DESC LIMIT 1",
            nativeQuery = true
    )
    public Double findLastHalfDayLeaveCount(Long employee_id);

    public List<Em_Leave> getEm_LeaveByEmployee_EmployeeId(Long employee_employeeId);

    @Query( value =
            "SELECT l.leave_id, l.approved_person, l.assigned_person, l.balanced_annual_leave, l.spend_annual_leave, l.from_date, l.to_date, l.employee_id, e.full_name, e.employee_number, e.email, e.nic_number " +
                    "FROM em_leave l " +
                    "INNER JOIN employee e " +
                    "ON l.employee_id = e.employee_id " +
                    "WHERE leave_type = 'ANNUAL' AND l.employee_id = ?1",
            nativeQuery = true
    )
    public List<Em_Leave> getAnnualLeave(Long employee_id);
}