package lk.code4x.hrsys.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 8:49 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long employeeId;
    private String employeeNumber;
    private String fullName;
    private String address;
    private String telephone;
    private String email;
    private String nicNumber;
    private String gender;
    private String dateOfBirth;
    private String educationLevel;
    private String freshmanOrNot;
    private String involvedDate;
    private String division;
    private String documents;
    private String thumbnail;
    @JsonIgnore
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Em_Leave> leaves = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Attendance> attendances = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Payment> payments = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Designation> designations = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Leave_Details> leaveDetails = new ArrayList<>();

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNicNumber() {
        return nicNumber;
    }

    public void setNicNumber(String nicNumber) {
        this.nicNumber = nicNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }

    public String getFreshmanOrNot() {
        return freshmanOrNot;
    }

    public void setFreshmanOrNot(String freshmanOrNot) {
        this.freshmanOrNot = freshmanOrNot;
    }

    public String getInvolvedDate() {
        return involvedDate;
    }

    public void setInvolvedDate(String involvedDate) {
        this.involvedDate = involvedDate;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<Em_Leave> getLeaves() {
        return leaves;
    }

    public void setLeaves(List<Em_Leave> leaves) {
        this.leaves = leaves;
    }

    public List<Attendance> getAttendances() {
        return attendances;
    }

    public void setAttendances(List<Attendance> attendances) {
        this.attendances = attendances;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public List<Designation> getDesignations() {
        return designations;
    }

    public void setDesignations(List<Designation> designations) {
        this.designations = designations;
    }

    public List<Leave_Details> getLeaveDetails() {
        return leaveDetails;
    }

    public void setLeaveDetails(List<Leave_Details> leaveDetails) {
        this.leaveDetails = leaveDetails;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId=" + employeeId +
                ", employeeNumber='" + employeeNumber + '\'' +
                ", fullName='" + fullName + '\'' +
                ", address='" + address + '\'' +
                ", telephone='" + telephone + '\'' +
                ", email='" + email + '\'' +
                ", nicNumber='" + nicNumber + '\'' +
                ", gender='" + gender + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", educationLevel='" + educationLevel + '\'' +
                ", freshmanOrNot='" + freshmanOrNot + '\'' +
                ", involvedDate='" + involvedDate + '\'' +
                ", division='" + division + '\'' +
                ", documents='" + documents + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", leaves=" + leaves +
                ", attendances=" + attendances +
                ", payments=" + payments +
                ", designations=" + designations +
                ", leaveDetails=" + leaveDetails +
                '}';
    }
}
