package lk.code4x.hrsys.model;

import javax.persistence.*;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 9:54 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Entity
public class Attendance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long attendance_id;

    @Column(nullable = false, length = 20)
    private String attendance_date;

    @Column(nullable = false, length = 20)
    private String in_time;

    @Column(nullable = false, length = 20)
    private String out_time;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "employeeId")
    private Employee employee;

    public Long getAttendance_id() {
        return attendance_id;
    }

    public void setAttendance_id(Long attendance_id) {
        this.attendance_id = attendance_id;
    }

    public String getAttendance_date() {
        return attendance_date;
    }

    public void setAttendance_date(String attendance_date) {
        this.attendance_date = attendance_date;
    }

    public String getIn_time() {
        return in_time;
    }

    public void setIn_time(String in_time) {
        this.in_time = in_time;
    }

    public String getOut_time() {
        return out_time;
    }

    public void setOut_time(String out_time) {
        this.out_time = out_time;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
