package lk.code4x.hrsys.model;

import javax.persistence.*;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 9:42 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Entity
public class Resigned_Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, length = 10, unique = true)
    private Long employeeId;

    @Column(nullable = false, unique = true)
    private String employeeNumber;

    @Column(nullable = false, length = 100)
    private String fullName;

    @Column(nullable = false, length = 10)
    private String telephone;

    @Column(nullable = false, length = 100)
    private String address;

    @Column(nullable = false, length = 50)
    private String email;

    @Column(nullable = false, length = 12)
    private String nic_number;

    @Column(nullable = false, length = 6)
    private String gender;

    @Column(nullable = false, length = 20)
    private String dateOfBirth;

    @Column(nullable = false, length = 2)
    private int resigned_age;

    @Column(nullable = false, length = 20)
    private String involvedDate;

    @Column(nullable = false, length = 100)
    private String division;

    @Column(nullable = false, length = 100)
    private double resigned_basic_salary;

    @Column(nullable = false, length = 10)
    private double resigned_allowance_salary;

    @Column(nullable = false, length = 100)
    private String position;

    @Column(nullable = true, length = 500)
    private String documents;

    @Column(nullable = true, length = 500)
    private String special_notes;

    @Column(nullable = true, length = 500)
    private String thumbnail;

    public Long getemployeeId() {
        return employeeId;
    }

    public void setemployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getemployeeNumber() {
        return employeeNumber;
    }

    public void setemployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getfullName() {
        return fullName;
    }

    public void setfullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNic_number() {
        return nic_number;
    }

    public void setNic_number(String nic_number) {
        this.nic_number = nic_number;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getdateOfBirth() {
        return dateOfBirth;
    }

    public void setdateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getResigned_age() {
        return resigned_age;
    }

    public void setResigned_age(int resigned_age) {
        this.resigned_age = resigned_age;
    }

    public String getinvolvedDate() {
        return involvedDate;
    }

    public void setinvolvedDate(String involvedDate) {
        this.involvedDate = involvedDate;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public double getResigned_basic_salary() {
        return resigned_basic_salary;
    }

    public void setResigned_basic_salary(double resigned_basic_salary) {
        this.resigned_basic_salary = resigned_basic_salary;
    }

    public double getResigned_allowance_salary() {
        return resigned_allowance_salary;
    }

    public void setResigned_allowance_salary(double resigned_allowance_salary) {
        this.resigned_allowance_salary = resigned_allowance_salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }

    public String getSpecial_notes() {
        return special_notes;
    }

    public void setSpecial_notes(String special_notes) {
        this.special_notes = special_notes;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
