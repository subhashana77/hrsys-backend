package lk.code4x.hrsys.model;

import javax.persistence.*;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 9:36 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Entity
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long payment_id;

    @Column(nullable = false, length = 20)
    private String payment_day;

    @Column(nullable = false, length = 10)
    private Double basic_salary;

    @Column(nullable = false, length = 10)
    private Double allowance_salary;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employeeId")
    private Employee employee;

    public Long getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(Long payment_id) {
        this.payment_id = payment_id;
    }

    public String getPayment_day() {
        return payment_day;
    }

    public void setPayment_day(String payment_day) {
        this.payment_day = payment_day;
    }

    public Double getBasic_salary() {
        return basic_salary;
    }

    public void setBasic_salary(Double basic_salary) {
        this.basic_salary = basic_salary;
    }

    public Double getAllowance_salary() {
        return allowance_salary;
    }

    public void setAllowance_salary(Double allowance_salary) {
        this.allowance_salary = allowance_salary;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
