package lk.code4x.hrsys.model;

import javax.persistence.*;

/**
 * @author dilshan.r
 * @created 23/03/2022 - 10:45 AM
 * @project hrsys-backend
 * @ide DoubleelliJ IDEA
 */

@Entity
public class Leave_Details {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long leaveDetailId;
    private Double annualLeaveCount;
    private Double casualLeaveCount;
    private Double medicalLeaveCount;
    private Double halfDayStatus;
    private Double workingMinutes;
    private Double shortInMinutes;
    private String inTime;
    private String outTime;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public Long getLeaveDetailId() {
        return leaveDetailId;
    }

    public void setLeaveDetailId(Long leaveDetailId) {
        this.leaveDetailId = leaveDetailId;
    }

    public Double getAnnualLeaveCount() {
        return annualLeaveCount;
    }

    public void setAnnualLeaveCount(Double annualLeaveCount) {
        this.annualLeaveCount = annualLeaveCount;
    }

    public Double getCasualLeaveCount() {
        return casualLeaveCount;
    }

    public void setCasualLeaveCount(Double casualLeaveCount) {
        this.casualLeaveCount = casualLeaveCount;
    }

    public Double getMedicalLeaveCount() {
        return medicalLeaveCount;
    }

    public void setMedicalLeaveCount(Double medicalLeaveCount) {
        this.medicalLeaveCount = medicalLeaveCount;
    }

    public Double getHalfDayStatus() {
        return halfDayStatus;
    }

    public void setHalfDayStatus(Double halfDayStatus) {
        this.halfDayStatus = halfDayStatus;
    }

    public Double getWorkingMinutes() {
        return workingMinutes;
    }

    public void setWorkingMinutes(Double workingMinutes) {
        this.workingMinutes = workingMinutes;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Double getShortInMinutes() {
        return shortInMinutes;
    }

    public void setShortInMinutes(Double shortInMinutes) {
        this.shortInMinutes = shortInMinutes;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }
}
