package lk.code4x.hrsys.model;

import javax.persistence.*;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 9:31 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


@Entity
public class Designation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long designation_id;

    @Column(nullable = false, length = 100)
    private String designation;

    @Column(nullable = false, length = 20)
    private String promotion_date;

    @Column(nullable = false, length = 10)
    private Double basic_salary;

    @Column(nullable = false, length = 10)
    private Double allowance_salary;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "employeeId")
    private Employee employee;

    public Long getDesignation_id() {
        return designation_id;
    }

    public void setDesignation_id(Long designation_id) {
        this.designation_id = designation_id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPromotion_date() {
        return promotion_date;
    }

    public void setPromotion_date(String promotion_date) {
        this.promotion_date = promotion_date;
    }

    public Double getBasic_salary() {
        return basic_salary;
    }

    public void setBasic_salary(Double basic_salary) {
        this.basic_salary = basic_salary;
    }

    public Double getAllowance_salary() {
        return allowance_salary;
    }

    public void setAllowance_salary(Double allowance_salary) {
        this.allowance_salary = allowance_salary;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
