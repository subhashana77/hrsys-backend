package lk.code4x.hrsys.dto;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 11:10 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public class DesignationDTO {
    private Long designation_id;
    private String designation;
    private String promotion_date;
    private Double basic_salary;
    private Double allowance_salary;
    private Long employeeId;

    public DesignationDTO() {
    }

    public DesignationDTO(Long designation_id, String designation, String promotion_date, Double basic_salary, Double allowance_salary, Long employeeId) {
        this.designation_id = designation_id;
        this.designation = designation;
        this.promotion_date = promotion_date;
        this.basic_salary = basic_salary;
        this.allowance_salary = allowance_salary;
        this.employeeId = employeeId;
    }

    public Long getDesignation_id() {
        return designation_id;
    }

    public void setDesignation_id(Long designation_id) {
        this.designation_id = designation_id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPromotion_date() {
        return promotion_date;
    }

    public void setPromotion_date(String promotion_date) {
        this.promotion_date = promotion_date;
    }

    public Double getBasic_salary() {
        return basic_salary;
    }

    public void setBasic_salary(Double basic_salary) {
        this.basic_salary = basic_salary;
    }

    public Double getAllowance_salary() {
        return allowance_salary;
    }

    public void setAllowance_salary(Double allowance_salary) {
        this.allowance_salary = allowance_salary;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "DesignationDTO{" +
                "designation_id=" + designation_id +
                ", designation='" + designation + '\'' +
                ", promotion_date='" + promotion_date + '\'' +
                ", basic_salary=" + basic_salary +
                ", allowance_salary=" + allowance_salary +
                ", employeeId=" + employeeId +
                '}';
    }
}
