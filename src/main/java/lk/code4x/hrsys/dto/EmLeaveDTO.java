package lk.code4x.hrsys.dto;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 11:12 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public class EmLeaveDTO {
    private Long leaveId;
    private String leaveType;
    private String fromDate;
    private String toDate;
    private String halfDate;
    private String half;
    private double halfCount;
    private String assignedPerson;
    private boolean assignedPersonApproval;
    private String approvedPerson;
    private Double spendMedicalLeave;
    private Double balancedMedicalLeave;
    private Double spendCasualLeave;
    private Double balancedCasualLeave;
    private Double spendAnnualLeave;
    private Double balancedAnnualLeave;
    private Double noPayLeave;
    private boolean status;
    private String shortDate;
    private String shortOutTime;
    private Long employeeId;

    public EmLeaveDTO() {
    }

    public EmLeaveDTO(Long leaveId, String leaveType, String fromDate, String toDate, String halfDate, String half, double halfCount, String assignedPerson, boolean assignedPersonApproval, String approvedPerson, Double spendMedicalLeave, Double balancedMedicalLeave, Double spendCasualLeave, Double balancedCasualLeave, Double spendAnnualLeave, Double balancedAnnualLeave, Double noPayLeave, boolean status, String shortDate, String shortOutTime, Long employeeId) {
        this.leaveId = leaveId;
        this.leaveType = leaveType;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.halfDate = halfDate;
        this.half = half;
        this.halfCount = halfCount;
        this.assignedPerson = assignedPerson;
        this.assignedPersonApproval = assignedPersonApproval;
        this.approvedPerson = approvedPerson;
        this.spendMedicalLeave = spendMedicalLeave;
        this.balancedMedicalLeave = balancedMedicalLeave;
        this.spendCasualLeave = spendCasualLeave;
        this.balancedCasualLeave = balancedCasualLeave;
        this.spendAnnualLeave = spendAnnualLeave;
        this.balancedAnnualLeave = balancedAnnualLeave;
        this.noPayLeave = noPayLeave;
        this.status = status;
        this.shortDate = shortDate;
        this.shortOutTime = shortOutTime;
        this.employeeId = employeeId;
    }

    public Long getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(Long leaveId) {
        this.leaveId = leaveId;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getAssignedPerson() {
        return assignedPerson;
    }

    public void setAssignedPerson(String assignedPerson) {
        this.assignedPerson = assignedPerson;
    }

    public boolean isAssignedPersonApproval() {
        return assignedPersonApproval;
    }

    public void setAssignedPersonApproval(boolean assignedPersonApproval) {
        this.assignedPersonApproval = assignedPersonApproval;
    }

    public String getApprovedPerson() {
        return approvedPerson;
    }

    public void setApprovedPerson(String approvedPerson) {
        this.approvedPerson = approvedPerson;
    }

    public Double getSpendMedicalLeave() {
        return spendMedicalLeave;
    }

    public void setSpendMedicalLeave(Double spendMedicalLeave) {
        this.spendMedicalLeave = spendMedicalLeave;
    }

    public Double getBalancedMedicalLeave() {
        return balancedMedicalLeave;
    }

    public void setBalancedMedicalLeave(Double balancedMedicalLeave) {
        this.balancedMedicalLeave = balancedMedicalLeave;
    }

    public Double getSpendCasualLeave() {
        return spendCasualLeave;
    }

    public void setSpendCasualLeave(Double spendCasualLeave) {
        this.spendCasualLeave = spendCasualLeave;
    }

    public Double getBalancedCasualLeave() {
        return balancedCasualLeave;
    }

    public void setBalancedCasualLeave(Double balancedCasualLeave) {
        this.balancedCasualLeave = balancedCasualLeave;
    }

    public Double getSpendAnnualLeave() {
        return spendAnnualLeave;
    }

    public void setSpendAnnualLeave(Double spendAnnualLeave) {
        this.spendAnnualLeave = spendAnnualLeave;
    }

    public Double getBalancedAnnualLeave() {
        return balancedAnnualLeave;
    }

    public void setBalancedAnnualLeave(Double balancedAnnualLeave) {
        this.balancedAnnualLeave = balancedAnnualLeave;
    }

    public Double getNoPayLeave() {
        return noPayLeave;
    }

    public void setNoPayLeave(Double noPayLeave) {
        this.noPayLeave = noPayLeave;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getHalfDate() {
        return halfDate;
    }

    public void setHalfDate(String halfDate) {
        this.halfDate = halfDate;
    }

    public String getHalf() {
        return half;
    }

    public void setHalf(String half) {
        this.half = half;
    }

    public double getHalfCount() {
        return halfCount;
    }

    public void setHalfCount(double halfCount) {
        this.halfCount = halfCount;
    }

    public String getShortDate() {
        return shortDate;
    }

    public void setShortDate(String shortDate) {
        this.shortDate = shortDate;
    }

    public String getShortOutTime() {
        return shortOutTime;
    }

    public void setShortOutTime(String shortOutTime) {
        this.shortOutTime = shortOutTime;
    }

    @Override
    public String toString() {
        return "Em_LeaveDTO{" +
                "leaveId=" + leaveId +
                ", leaveType='" + leaveType + '\'' +
                ", fromDate='" + fromDate + '\'' +
                ", toDate='" + toDate + '\'' +
                ", halfDate='" + halfDate + '\'' +
                ", half='" + half + '\'' +
                ", halfCount=" + halfCount +
                ", assignedPerson='" + assignedPerson + '\'' +
                ", assignedPersonApproval=" + assignedPersonApproval +
                ", approvedPerson='" + approvedPerson + '\'' +
                ", spendMedicalLeave=" + spendMedicalLeave +
                ", balancedMedicalLeave=" + balancedMedicalLeave +
                ", spendCasualLeave=" + spendCasualLeave +
                ", balancedCasualLeave=" + balancedCasualLeave +
                ", spendAnnualLeave=" + spendAnnualLeave +
                ", balancedAnnualLeave=" + balancedAnnualLeave +
                ", noPayLeave=" + noPayLeave +
                ", status=" + status +
                ", shortDate='" + shortDate + '\'' +
                ", shortOutTime='" + shortOutTime + '\'' +
                ", employeeId=" + employeeId +
                '}';
    }
}
