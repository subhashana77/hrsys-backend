package lk.code4x.hrsys.dto;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 11:09 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public class AttendanceDTO {
    private Long attendance_id;
    private String attendance_date;
    private String in_time;
    private String out_time;
    private Long employeeId;

    public AttendanceDTO() {
    }

    public AttendanceDTO(Long attendance_id, String attendance_date, String in_time, String out_time, Long employeeId) {
        this.attendance_id = attendance_id;
        this.attendance_date = attendance_date;
        this.in_time = in_time;
        this.out_time = out_time;
        this.employeeId = employeeId;
    }

    public Long getAttendance_id() {
        return attendance_id;
    }

    public void setAttendance_id(Long attendance_id) {
        this.attendance_id = attendance_id;
    }

    public String getAttendance_date() {
        return attendance_date;
    }

    public void setAttendance_date(String attendance_date) {
        this.attendance_date = attendance_date;
    }

    public String getIn_time() {
        return in_time;
    }

    public void setIn_time(String in_time) {
        this.in_time = in_time;
    }

    public String getOut_time() {
        return out_time;
    }

    public void setOut_time(String out_time) {
        this.out_time = out_time;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "AttendanceDTO{" +
                "attendance_id=" + attendance_id +
                ", attendance_date='" + attendance_date + '\'' +
                ", in_time='" + in_time + '\'' +
                ", out_time='" + out_time + '\'' +
                ", employeeId=" + employeeId +
                '}';
    }
}
