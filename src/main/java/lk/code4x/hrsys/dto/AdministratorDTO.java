package lk.code4x.hrsys.dto;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 11:06 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public class AdministratorDTO {
    private Long administrator_id;
    private String email;
    private String password;

    public AdministratorDTO() {
    }

    public AdministratorDTO(Long administrator_id, String email, String password, String login_date_and_time, String logout_date_and_time) {
        this.administrator_id = administrator_id;
        this.email = email;
        this.password = password;
    }

    public Long getAdministrator_id() {
        return administrator_id;
    }

    public void setAdministrator_id(Long administrator_id) {
        this.administrator_id = administrator_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "AdministratorDTO{" +
                "administrator_id=" + administrator_id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
