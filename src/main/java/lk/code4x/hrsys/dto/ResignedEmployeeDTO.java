package lk.code4x.hrsys.dto;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 11:15 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public class ResignedEmployeeDTO {
    private Long employeeId;
    private String employeeNumber;
    private String fullName;
    private String telephone;
    private String address;
    private String email;
    private String nic_number;
    private String gender;
    private String dateOfBirth;
    private int resigned_age;
    private String involvedDate;
    private String division;
    private double resigned_basic_salary;
    private double resigned_allowance_salary;
    private String position;
    private String documents;
    private String special_notes;
    private String thumbnail;

    public ResignedEmployeeDTO() {
    }

    public ResignedEmployeeDTO(Long employeeId, String employeeNumber, String fullName, String telephone, String address, String email, String nic_number, String gender, String dateOfBirth, int resigned_age, String involvedDate, String division, double resigned_basic_salary, double resigned_allowance_salary, String position, String documents, String special_notes, String thumbnail) {
        this.employeeId = employeeId;
        this.employeeNumber = employeeNumber;
        this.fullName = fullName;
        this.telephone = telephone;
        this.address = address;
        this.email = email;
        this.nic_number = nic_number;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.resigned_age = resigned_age;
        this.involvedDate = involvedDate;
        this.division = division;
        this.resigned_basic_salary = resigned_basic_salary;
        this.resigned_allowance_salary = resigned_allowance_salary;
        this.position = position;
        this.documents = documents;
        this.special_notes = special_notes;
        this.thumbnail = thumbnail;
    }

    public Long getemployeeId() {
        return employeeId;
    }

    public void setemployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getemployeeNumber() {
        return employeeNumber;
    }

    public void setemployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getfullName() {
        return fullName;
    }

    public void setfullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNic_number() {
        return nic_number;
    }

    public void setNic_number(String nic_number) {
        this.nic_number = nic_number;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getdateOfBirth() {
        return dateOfBirth;
    }

    public void setdateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getResigned_age() {
        return resigned_age;
    }

    public void setResigned_age(int resigned_age) {
        this.resigned_age = resigned_age;
    }

    public String getinvolvedDate() {
        return involvedDate;
    }

    public void setinvolvedDate(String involvedDate) {
        this.involvedDate = involvedDate;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public double getResigned_basic_salary() {
        return resigned_basic_salary;
    }

    public void setResigned_basic_salary(double resigned_basic_salary) {
        this.resigned_basic_salary = resigned_basic_salary;
    }

    public double getResigned_allowance_salary() {
        return resigned_allowance_salary;
    }

    public void setResigned_allowance_salary(double resigned_allowance_salary) {
        this.resigned_allowance_salary = resigned_allowance_salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }

    public String getSpecial_notes() {
        return special_notes;
    }

    public void setSpecial_notes(String special_notes) {
        this.special_notes = special_notes;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "Resigned_EmployeeDTO{" +
                "employeeId=" + employeeId +
                ", employeeNumber='" + employeeNumber + '\'' +
                ", fullName='" + fullName + '\'' +
                ", telephone='" + telephone + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", nic_number='" + nic_number + '\'' +
                ", gender='" + gender + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", resigned_age=" + resigned_age +
                ", involvedDate='" + involvedDate + '\'' +
                ", division='" + division + '\'' +
                ", resigned_basic_salary=" + resigned_basic_salary +
                ", resigned_allowance_salary=" + resigned_allowance_salary +
                ", position='" + position + '\'' +
                ", documents='" + documents + '\'' +
                ", special_notes='" + special_notes + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}
