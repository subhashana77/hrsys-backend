package lk.code4x.hrsys.dto;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 11:14 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public class PaymentDTO {
    private Long payment_id;
    private String payment_day;
    private Double basic_salary;
    private Double allowance_salary;
    private Long employeeId;

    public PaymentDTO() {
    }

    public PaymentDTO(Long payment_id, String payment_day, Double basic_salary, Double allowance_salary, Long employeeId) {
        this.payment_id = payment_id;
        this.payment_day = payment_day;
        this.basic_salary = basic_salary;
        this.allowance_salary = allowance_salary;
        this.employeeId = employeeId;
    }

    public Long getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(Long payment_id) {
        this.payment_id = payment_id;
    }

    public String getPayment_day() {
        return payment_day;
    }

    public void setPayment_day(String payment_day) {
        this.payment_day = payment_day;
    }

    public Double getBasic_salary() {
        return basic_salary;
    }

    public void setBasic_salary(Double basic_salary) {
        this.basic_salary = basic_salary;
    }

    public Double getAllowance_salary() {
        return allowance_salary;
    }

    public void setAllowance_salary(Double allowance_salary) {
        this.allowance_salary = allowance_salary;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "PaymentDTO{" +
                "payment_id=" + payment_id +
                ", payment_day='" + payment_day + '\'' +
                ", basic_salary=" + basic_salary +
                ", allowance_salary=" + allowance_salary +
                ", employeeId=" + employeeId +
                '}';
    }
}
