package lk.code4x.hrsys.dto;

/**
 * @author dilshan.r
 * @created 30/03/2022 - 2:48 PM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public class AnnualLeaveDTO {

    private Long leaveId;
    private String approvedPerson;
    private String assignPerson;
    private double balancedAnnualLeave;
    private double spendAnnualLeave;
    private String leaveFromDate;
    private String leaveToDate;
    private Long employeeId;
    private String employeeName;
    private String employeeNumber;
    private String employeeEmail;
    private String employeeNic;

    public AnnualLeaveDTO() {
    }

    public AnnualLeaveDTO(Long leaveId, String approvedPerson, String assignPerson, double balancedAnnualLeave, double spendAnnualLeave, String leaveFromDate, String leaveToDate, Long employeeId, String employeeName, String employeeNumber, String employeeEmail, String employeeNic) {
        this.leaveId = leaveId;
        this.approvedPerson = approvedPerson;
        this.assignPerson = assignPerson;
        this.balancedAnnualLeave = balancedAnnualLeave;
        this.spendAnnualLeave = spendAnnualLeave;
        this.leaveFromDate = leaveFromDate;
        this.leaveToDate = leaveToDate;
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeNumber = employeeNumber;
        this.employeeEmail = employeeEmail;
        this.employeeNic = employeeNic;
    }

    public Long getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(Long leaveId) {
        this.leaveId = leaveId;
    }

    public String getApprovedPerson() {
        return approvedPerson;
    }

    public void setApprovedPerson(String approvedPerson) {
        this.approvedPerson = approvedPerson;
    }

    public String getAssignPerson() {
        return assignPerson;
    }

    public void setAssignPerson(String assignPerson) {
        this.assignPerson = assignPerson;
    }

    public double getBalancedAnnualLeave() {
        return balancedAnnualLeave;
    }

    public void setBalancedAnnualLeave(double balancedAnnualLeave) {
        this.balancedAnnualLeave = balancedAnnualLeave;
    }

    public double getSpendAnnualLeave() {
        return spendAnnualLeave;
    }

    public void setSpendAnnualLeave(double spendAnnualLeave) {
        this.spendAnnualLeave = spendAnnualLeave;
    }

    public String getLeaveFromDate() {
        return leaveFromDate;
    }

    public void setLeaveFromDate(String leaveFromDate) {
        this.leaveFromDate = leaveFromDate;
    }

    public String getLeaveToDate() {
        return leaveToDate;
    }

    public void setLeaveToDate(String leaveToDate) {
        this.leaveToDate = leaveToDate;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeeNic() {
        return employeeNic;
    }

    public void setEmployeeNic(String employeeNic) {
        this.employeeNic = employeeNic;
    }
}
