package lk.code4x.hrsys.dto;

/**
 * @author dilshan.r
 * @created 11/03/2022 - 11:11 AM
 * @project hrsys-backend
 * @ide IntelliJ IDEA
 */


public class EmployeeDTO {
    private Long employeeId;
    private String employeeNumber;
    private String fullName;
    private String address;
    private String telephone;
    private String email;
    private String nicNumber;
    private String gender;
    private String dateOfBirth;
    private String educationLevel;
    private String freshmanOrNot;
    private String involvedDate;
    private String division;
    private String documents;
    private String thumbnail;

    public EmployeeDTO() {
    }

    public EmployeeDTO(Long employeeId, String employeeNumber, String fullName, String address, String telephone, String email, String nicNumber, String gender, String dateOfBirth, String educationLevel, String freshmanOrNot, String involvedDate, String division, String documents, String thumbnail) {
        this.employeeId = employeeId;
        this.employeeNumber = employeeNumber;
        this.fullName = fullName;
        this.address = address;
        this.telephone = telephone;
        this.email = email;
        this.nicNumber = nicNumber;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.educationLevel = educationLevel;
        this.freshmanOrNot = freshmanOrNot;
        this.involvedDate = involvedDate;
        this.division = division;
        this.documents = documents;
        this.thumbnail = thumbnail;
    }

    public Long getemployeeId() {
        return employeeId;
    }

    public void setemployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getemployeeNumber() {
        return employeeNumber;
    }

    public void setemployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getfullName() {
        return fullName;
    }

    public void setfullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getnicNumber() {
        return nicNumber;
    }

    public void setnicNumber(String nicNumber) {
        this.nicNumber = nicNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getdateOfBirth() {
        return dateOfBirth;
    }

    public void setdateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String geteducationLevel() {
        return educationLevel;
    }

    public void seteducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }

    public String getfreshmanOrNot() {
        return freshmanOrNot;
    }

    public void setfreshmanOrNot(String freshmanOrNot) {
        this.freshmanOrNot = freshmanOrNot;
    }

    public String getinvolvedDate() {
        return involvedDate;
    }

    public void setinvolvedDate(String involvedDate) {
        this.involvedDate = involvedDate;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "EmployeeDTO{" +
                "employeeId=" + employeeId +
                ", employeeNumber='" + employeeNumber + '\'' +
                ", fullName='" + fullName + '\'' +
                ", address='" + address + '\'' +
                ", telephone='" + telephone + '\'' +
                ", email='" + email + '\'' +
                ", nicNumber='" + nicNumber + '\'' +
                ", gender='" + gender + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", educationLevel='" + educationLevel + '\'' +
                ", freshmanOrNot='" + freshmanOrNot + '\'' +
                ", involvedDate='" + involvedDate + '\'' +
                ", division='" + division + '\'' +
                ", documents='" + documents + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}
