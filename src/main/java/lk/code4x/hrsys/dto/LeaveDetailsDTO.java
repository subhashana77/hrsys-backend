package lk.code4x.hrsys.dto;

/**
 * @author dilshan.r
 * @created 23/03/2022 - 11:15 AM
 * @project hrsys-backend
 * @ide DoubleelliJ IDEA
 */


public class LeaveDetailsDTO {
    private Long leaveDetailId;
    private Double annualLeaveCount;
    private Double casualLeaveCount;
    private Double medicalLeaveCount;
    private Double halfDayStatus;
    private Double workingMinutes;
    private Double shortInMinutes;
    private String inTime;
    private String outTime;
    private Long employeeId;

    public LeaveDetailsDTO() {
    }

    public LeaveDetailsDTO(Long leaveDetailId, Double annualLeaveCount, Double casualLeaveCount, Double medicalLeaveCount, Double halfDayStatus, Double workingMinutes, Double shortInMinutes, String inTime, String outTime, Long employeeId) {
        this.leaveDetailId = leaveDetailId;
        this.annualLeaveCount = annualLeaveCount;
        this.casualLeaveCount = casualLeaveCount;
        this.medicalLeaveCount = medicalLeaveCount;
        this.halfDayStatus = halfDayStatus;
        this.workingMinutes = workingMinutes;
        this.shortInMinutes = shortInMinutes;
        this.inTime = inTime;
        this.outTime = outTime;
        this.employeeId = employeeId;
    }

    public Long getLeaveDetailId() {
        return leaveDetailId;
    }

    public void setLeaveDetailId(Long leaveDetailId) {
        this.leaveDetailId = leaveDetailId;
    }

    public Double getAnnualLeaveCount() {
        return annualLeaveCount;
    }

    public void setAnnualLeaveCount(Double annualLeaveCount) {
        this.annualLeaveCount = annualLeaveCount;
    }

    public Double getCasualLeaveCount() {
        return casualLeaveCount;
    }

    public void setCasualLeaveCount(Double casualLeaveCount) {
        this.casualLeaveCount = casualLeaveCount;
    }

    public Double getMedicalLeaveCount() {
        return medicalLeaveCount;
    }

    public void setMedicalLeaveCount(Double medicalLeaveCount) {
        this.medicalLeaveCount = medicalLeaveCount;
    }

    public Double getHalfDayStatus() {
        return halfDayStatus;
    }

    public void setHalfDayStatus(Double halfDayStatus) {
        this.halfDayStatus = halfDayStatus;
    }

    public Double getWorkingMinutes() {
        return workingMinutes;
    }

    public void setWorkingMinutes(Double workingMinutes) {
        this.workingMinutes = workingMinutes;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public Double getShortInMinutes() {
        return shortInMinutes;
    }

    public void setShortInMinutes(Double shortInMinutes) {
        this.shortInMinutes = shortInMinutes;
    }

    @Override
    public String toString() {
        return "Leave_Details_DTO{" +
                "leaveDetailId=" + leaveDetailId +
                ", annualLeaveCount=" + annualLeaveCount +
                ", casualLeaveCount=" + casualLeaveCount +
                ", medicalLeaveCount=" + medicalLeaveCount +
                ", halfDayStatus='" + halfDayStatus + '\'' +
                ", workingMinutes=" + workingMinutes +
                ", shortInMinutes='" + shortInMinutes + '\'' +
                ", inTime='" + inTime + '\'' +
                ", outTime='" + outTime + '\'' +
                ", employeeId=" + employeeId +
                '}';
    }
}
