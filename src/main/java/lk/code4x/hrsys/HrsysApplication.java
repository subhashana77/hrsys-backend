package lk.code4x.hrsys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication()
public class HrsysApplication {

	public static void main(String[] args) {
		SpringApplication.run(HrsysApplication.class, args);
	}

}
